"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.Channel = void 0;
var chalk = require("chalk");
var events_1 = require("events");
var Logger_1 = require("./Logger");
var Channel = /** @class */ (function (_super) {
    __extends(Channel, _super);
    function Channel(_id, server) {
        var _this = _super.call(this) || this;
        _this._id = _id;
        _this.connectedClients = [];
        _this.server = server;
        _this.logger = new Logger_1.Logger(_id, chalk.yellow);
        _this.server.channels.push(_this);
        return _this;
    }
    Channel.prototype.bindEventListeners = function () {
        var _this = this;
        this.on('a', function (msg) {
            if (!msg.message)
                return;
            _this.logger.log("Received chat message: " + msg.message);
            _this.connectedClients.forEach(function (cl) {
                cl.sendArray([{
                        m: 'a',
                        a: msg.message
                    }]);
            });
        });
    };
    Channel.prototype.connectClient = function (cl) {
        if (this.hasClient(cl)) {
        }
        else {
            this.connectedClients.push(cl);
            cl.sendChannelMessage(this);
        }
        cl.channel = this;
    };
    Channel.prototype.isLobby = function () {
        if (this._id == 'lobby' || this._id.startsWith('lobby') && !isNaN(parseInt(this._id.substr(5, 2))))
            return true;
        if (this._id.startsWith('test/') && this._id !== 'test/')
            return true;
        return false;
    };
    Channel.prototype.hasClient = function (cl) {
        for (var _i = 0, _a = this.connectedClients; _i < _a.length; _i++) {
            var c = _a[_i];
            if (cl.id == c.id) {
                return true;
            }
        }
        return false;
    };
    Channel.prototype.getIncrementedIDNumber = function () {
        var num;
        var gotNum = parseInt(this._id.substr(5));
        var idHasNum = typeof gotNum == 'number';
        if (idHasNum) {
            return gotNum++;
        }
        else {
            return 1;
        }
    };
    return Channel;
}(events_1.EventEmitter));
exports.Channel = Channel;
