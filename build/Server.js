"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.Server = void 0;
var chalk = require("chalk");
var events_1 = require("events");
var Logger_1 = require("./Logger");
var WebSocketHandler_1 = require("./WebSocketHandler");
var WebServer_1 = require("./WebServer");
var Channel_1 = require("./Channel");
var PORT = process.env.PORT;
var Server = /** @class */ (function (_super) {
    __extends(Server, _super);
    function Server() {
        var _this = _super.call(this) || this;
        _this.bindEventListeners();
        _this.channels = [];
        _this.logger = new Logger_1.Logger('server', chalk.green);
        _this.logger.debug("port: " + PORT);
        _this.wsh = new WebSocketHandler_1.WebSocketHandler(_this);
        _this.ws = new WebServer_1.WebServer(parseInt(PORT));
        _this.ws.linkWebSocketServer(_this.wsh.wss);
        _this.emit('start');
        return _this;
    }
    Server.prototype.bindEventListeners = function () {
        var _this = this;
        this.on('start', function () {
            _this.logger.log('Server started.');
        });
    };
    Server.prototype.setClientChannel = function (cl, str) {
        // TODO put client in channel or create new channel
        var channelExists = false;
        var ch;
        for (var _i = 0, _a = this.channels; _i < _a.length; _i++) {
            var c = _a[_i];
            if (str == c._id)
                channelExists = true;
            ch = c;
        }
        if (!channelExists) {
            ch = new Channel_1.Channel(str, this);
        }
        if (ch.hasClient(cl))
            return;
        if (ch.isLobby() && ch.connectedClients.length >= 20) {
            var new_id_1 = ch._id + ch.getIncrementedIDNumber();
            ch = this.channels.find(function (ch) { return new_id_1 == str; });
            if (!ch)
                ch = new Channel_1.Channel(new_id_1, this);
        }
        ch.connectClient(cl);
    };
    return Server;
}(events_1.EventEmitter));
exports.Server = Server;
