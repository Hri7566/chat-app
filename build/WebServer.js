"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.WebServer = void 0;
var express = require("express");
var events_1 = require("events");
var path_1 = require("path");
var Logger_1 = require("./Logger");
var WebServer = /** @class */ (function (_super) {
    __extends(WebServer, _super);
    function WebServer(port) {
        if (port === void 0) { port = 3000; }
        var _this = _super.call(this) || this;
        _this.logger = new Logger_1.Logger("Webserver");
        _this.bindEventListeners();
        _this.app = express();
        _this.app.use(express.static((0, path_1.join)(process.cwd(), 'dist')));
        _this.server = _this.app.listen(port, function () { });
        _this.emit('start');
        return _this;
    }
    WebServer.prototype.bindEventListeners = function () {
        var _this = this;
        this.on('start', function () {
            _this.logger.log('Express server started');
        });
    };
    WebServer.prototype.linkWebSocketServer = function (wss) {
        this.server.on('upgrade', function (request, socket, head) {
            wss.handleUpgrade(request, socket, head, function (ws, req) {
                wss.emit('connection', ws, req);
            });
        });
    };
    return WebServer;
}(events_1.EventEmitter));
exports.WebServer = WebServer;
