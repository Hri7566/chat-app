"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.Client = void 0;
var events_1 = require("events");
var Client = /** @class */ (function (_super) {
    __extends(Client, _super);
    function Client(id, ws, server) {
        var _this = _super.call(this) || this;
        _this.id = id;
        _this.server = server;
        _this.ws = ws;
        _this.bindEventListeners();
        _this.server.setClientChannel(_this, "lobby");
        return _this;
    }
    Client.prototype.bindEventListeners = function () {
        var _this = this;
        this.ws.on('message', function (data) {
            try {
                var msgs = JSON.parse(data.toString());
                for (var _i = 0, msgs_1 = msgs; _i < msgs_1.length; _i++) {
                    var msg = msgs_1[_i];
                    _this.emit(msg.m, msg, false);
                }
            }
            catch (err) {
                console.error(err);
            }
        });
        this.on('hi', function (msg) {
            _this.sendArray([{
                    m: 'hi'
                }]);
        });
        this.on('a', function (msg) {
            if (!msg.message)
                return;
            _this.receiveChatMessage(msg);
        });
        // TODO user data, other messages, maybe cursors
    };
    Client.prototype.sendArray = function (arr) {
        var jmsgs = JSON.stringify(arr);
        this.ws.send(jmsgs);
    };
    Client.prototype.receiveChatMessage = function (msg) {
        // console.log(`Received chat message: ${msg.message}`);
        // this.server.wsh.clients.forEach((cl, id) => {
        //     cl.sendArray([{m:'a', a: msg.message}])
        // });
        this.channel.emit('a', msg);
    };
    Client.prototype.sendChannelMessage = function (ch) {
        this.sendArray([{
                m: "ch"
            }]);
    };
    Client.prototype.setChannel = function (str) {
    };
    return Client;
}(events_1.EventEmitter));
exports.Client = Client;
