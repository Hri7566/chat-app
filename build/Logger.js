"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
exports.__esModule = true;
exports.Logger = void 0;
var events_1 = require("events");
var chalk = require("chalk");
var DEBUG = process.env.DEBUG;
var Logger = /** @class */ (function (_super) {
    __extends(Logger, _super);
    function Logger(id, color) {
        if (color === void 0) { color = chalk.green; }
        var _this = _super.call(this) || this;
        _this.id = id;
        _this.color = color;
        return _this;
    }
    Logger.prototype.log = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        this.print.apply(this, __spreadArray([console.log, chalk.bgBlue, 'info'], args, false));
    };
    Logger.prototype.warn = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        this.print.apply(this, __spreadArray([console.warn, chalk.bgYellow, 'warn'], args, false));
    };
    Logger.prototype.error = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        this.print.apply(this, __spreadArray([console.warn, chalk.bgRed, 'error'], args, false));
    };
    Logger.prototype.debug = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (DEBUG == "true") {
            this.print.apply(this, __spreadArray([console.debug, chalk.bgBlue, 'debug'], args, false));
        }
    };
    Logger.prototype.print = function (method, color, name) {
        var strs = [];
        for (var _i = 3; _i < arguments.length; _i++) {
            strs[_i - 3] = arguments[_i];
        }
        method.apply(void 0, __spreadArray([color(name) + " " + this.color(this.id)], strs, false));
    };
    return Logger;
}(events_1.EventEmitter));
exports.Logger = Logger;
