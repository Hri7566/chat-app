import WebSocket = require('ws');
import { EventEmitter } from 'events';
import { Logger } from './Logger';
import chalk = require('chalk');
import { Client } from './Client';
import { Server } from './Server';

class WebSocketHandler extends EventEmitter {
    wss: WebSocket.Server;
    logger: Logger;
    clients: Map<number, Client>;
    server: Server;

    constructor (server: Server) {
        super();

        this.server = server;
        this.bindEventListeners();
        
        this.logger = new Logger('WebSocketHandler', chalk.yellow);
        this.clients = new Map();

        this.emit('startwss');
        this.emit('ready');
    }
    
    bindEventListeners() {
        this.on('ready', () => {
            this.logger.log('WebSocket handler ready.');
        });
        
        this.on('startwss', () => {
            this.logger.log('Starting WebSocket server...');
            let nextClientID = 0
            
            this.wss = new WebSocket.Server({
                noServer: true
            });
            
            this.wss.on('connection', (ws, req) => {
                this.clients.set(nextClientID, new Client(nextClientID, ws, this.server));
                nextClientID++;
            });
            
            this.wss.on('close', () => {
                this.logger.warn('WebSocket server closed.');
            });
        });
    }
}

export {
    WebSocketHandler
}
