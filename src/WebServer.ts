import express = require('express');
import { Express } from 'express';
import { EventEmitter } from 'events';
import { Server as HTTPServer } from 'http';
import { Server as HTTPSServer } from 'https';
import WebSocket = require('ws');
import { Socket } from 'net';
import { join } from 'path';

import { Logger } from './Logger';

class WebServer extends EventEmitter {
    app: Express;
    logger: Logger;
    server: HTTPServer | HTTPSServer;

    constructor (port: number = 3000) {
        super();

        this.logger = new Logger("Webserver");

        this.bindEventListeners();
        
        this.app = express();
        this.app.use(express.static(join(process.cwd(), 'dist')));
        this.server = this.app.listen(port, () => {});

        this.emit('start');
    }

    bindEventListeners() {
        this.on('start', () => {
            this.logger.log('Express server started');
        });
    }

    linkWebSocketServer(wss: WebSocket.Server) {
        this.server.on('upgrade', (request, socket, head) => {
            wss.handleUpgrade(request, (socket as Socket), head, (ws, req) => {
                wss.emit('connection', ws, req);
            });
        });
    }
}

export {
    WebServer
}
