import WebSocket = require('ws');
import chalk = require('chalk');

import { EventEmitter } from 'events';
import { Logger } from './Logger';
import { WebSocketHandler } from './WebSocketHandler';
import { WebServer } from './WebServer';
import { Channel } from './Channel';
import { Client } from './Client';

const PORT = process.env.PORT;

class Server extends EventEmitter {
    logger: Logger;
    wsh: WebSocketHandler;
    ws: WebServer;
    channels: Channel[];

    constructor () {
        super();

        this.bindEventListeners();

        this.channels = [];

        this.logger = new Logger('server', chalk.green);
        this.logger.debug(`port: ${PORT}`);
        this.wsh = new WebSocketHandler(this);
        this.ws = new WebServer(parseInt(PORT));

        this.ws.linkWebSocketServer(this.wsh.wss);
        
        this.emit('start');
    }

    bindEventListeners() {
        this.on('start', () => {
            this.logger.log('Server started.');
        });
    }

    setClientChannel(cl: Client, str: string) {
        // TODO put client in channel or create new channel
        
        let channelExists = false;
        let ch: Channel;

        for (let c of this.channels) {
            if (str == c._id) channelExists = true;
            ch = c;
        }

        
        if (!channelExists) {
            ch = new Channel(str, this);
        }
        
        if (ch.hasClient(cl)) return;

        
        if (ch.isLobby() && ch.connectedClients.length >= 20) {
            let new_id = ch._id + ch.getIncrementedIDNumber();
            ch = this.channels.find(ch => new_id == str);
            if (!ch) ch = new Channel(new_id, this);
        }

        ch.connectClient(cl);
    }
}

export {
    Server
}
