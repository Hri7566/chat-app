import { EventEmitter } from 'events';
import chalk = require('chalk');

const DEBUG = process.env.DEBUG;

class Logger extends EventEmitter {
    id: string;
    color: Function;

    constructor (id: string, color: Function = chalk.green) {
        super();

        this.id = id;
        this.color = color;
    }

    log(...args: any[]) {
        this.print(console.log, chalk.bgBlue, 'info', ...args);
    }
    
    warn(...args: any[]) {
        this.print(console.warn, chalk.bgYellow, 'warn', ...args);
    }
    
    error(...args: any[]) {
        this.print(console.warn, chalk.bgRed, 'error', ...args);
    }
    
    debug(...args: any[]) {
        if (DEBUG == "true") {
            this.print(console.debug, chalk.bgBlue, 'debug', ...args);
        }
    }

    print(method: Function, color: Function, name: string, ...strs: string[]) {
        method(`${color(name)} ${this.color(this.id)}`, ...strs);
    }
}

export {
    Logger
}
