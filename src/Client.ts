import { EventEmitter } from 'events';
import WebSocket = require('ws');
import { Channel } from './Channel';
import { Server } from './Server';

class Client extends EventEmitter {
    id: string | number;
    ws: WebSocket;
    server: Server;
    channel: Channel;

    constructor (id: string | number, ws: WebSocket, server: Server) {
        super();

        this.id = id;

        this.server = server;
        this.ws = ws;

        this.bindEventListeners();

        this.server.setClientChannel(this, "lobby");
    }

    bindEventListeners() {
        this.ws.on('message', data => {
            try {
                let msgs: any[] = JSON.parse(data.toString());
                for (let msg of msgs) {
                    this.emit(msg.m, msg, false);
                }
            } catch (err) {
                console.error(err);
            }
        });
        
        this.on('hi', msg => {
            this.sendArray([{
                m: 'hi'
            }]);
        });

        this.on('a', msg => {
            if (!msg.message) return;

            this.receiveChatMessage(msg);
        });

        // TODO user data, other messages, maybe cursors
    }

    sendArray(arr) {
        let jmsgs = JSON.stringify(arr);
        this.ws.send(jmsgs);
    }

    receiveChatMessage(msg) { // TODO move this to Channel
        // console.log(`Received chat message: ${msg.message}`);

        // this.server.wsh.clients.forEach((cl, id) => {
        //     cl.sendArray([{m:'a', a: msg.message}])
        // });

        this.channel.emit('a', msg);
    }

    sendChannelMessage(ch: Channel) {
        this.sendArray([{
            m: "ch",
            _id: ch._id
        }]);
    }

    setChannel(str: string) {
        
    }
}

export {
    Client
}
