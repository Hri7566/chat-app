import chalk = require("chalk");
import { EventEmitter } from "events";
import { Client } from "./Client";
import { Logger } from "./Logger";
import { Server } from "./Server";

class Channel extends EventEmitter {
    _id: string;
    connectedClients: Client[];
    server: Server;
    logger: Logger;

    constructor (_id: string, server: Server) {
        super();
        this._id = _id;
        this.connectedClients = [];
        this.server = server;

        this.logger = new Logger(_id, chalk.yellow);

        this.server.channels.push(this);
    }

    bindEventListeners() {
        this.on('a', msg => {
            if (!msg.message) return;

            this.logger.log(`Received chat message: ${msg.message}`);

            this.connectedClients.forEach(cl => {
                cl.sendArray([{
                    m: 'a',
                    a: msg.message
                }]);
            });
        });
    }

    connectClient(cl: Client) {
        if (this.hasClient(cl)) {

        } else {
            this.connectedClients.push(cl);
            cl.sendChannelMessage(this);
        }

        cl.channel = this;
    }

    isLobby(): boolean {
        if (this._id == 'lobby' || this._id.startsWith('lobby') && !isNaN(parseInt(this._id.substr(5, 2)))) return true;
        if (this._id.startsWith('test/') && this._id !== 'test/') return true;
        return false;
    }

    hasClient(cl: Client) {
        for (let c of this.connectedClients) {
            if (cl.id == c.id) {
                return true;
            }
        }

        return false;
    }

    getIncrementedIDNumber(): number {
        let num: number;
        let gotNum: number = parseInt(this._id.substr(5));
        let idHasNum: boolean = typeof gotNum == 'number';

        if (idHasNum) {
            return gotNum++;
        } else {
            return 1;
        }
    }

    getPublicInfo() {
        
    }
}

export {
    Channel
}
