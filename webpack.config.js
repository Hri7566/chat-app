const HtmlWebpackPlugin = require('html-webpack-plugin');

const { join } = require('path');

globalThis.__approot = process.cwd();

module.exports = {
    mode: "development",
    context: __approot,
    entry: join(__approot, "webpack/index.ts"),
    output: {
        path: join(__approot, 'dist'),
        filename: 'index.js'
    },
    resolve: {
        extensions: [
            '.js',
            '.ts'
        ]
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: [/node_modules/],
                loader: "ts-loader",
                options: {
                    configFile: "tsconfig.webpack.json"
                }
            },
            {
                test: /\.s[ac]ss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.html$/,
                loader: 'html-loader',
                options: {
                    sources: false
                }
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'webpack/index.html'
        })
    ]
}
